with Ada.Command_Line ;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;

with interfaces.C ; use Interfaces.C;
with Interfaces.C.Strings ; use Interfaces.C.Strings ;

with openssl.evp.cipher ; use openssl.evp.cipher ;
with openssl.evp.cipher.util ;

procedure Sslcrypt is

   algname : string := "aes-256-cbc" ;
   --key : string := ada.Command_Line.Argument(2) ;
   --filename : string := ada.Command_Line.Argument(3) ;

   cip : openssl.evp.cipher.CIPHER_ALGORITHM ;
   iv : openssl.evp.cipher.InitVector_Type ;
   status : int ;
begin
   --  Put_Line(algname);
   --  cip := openssl.evp.cipher.cipherbyname(algname) ;
   --  if cip = openssl.evp.cipher.NullCipher
   --  then
   --     Put_Line("Algorithm name is not recognized");
   --     return ;
   --  end if ;
   --  Put_Line("Found the algorithm");
   --  Put("Block Size "); Put( integer(openssl.evp.cipher.Block_Size(cip))) ; New_Line ;
   --  Put("Key Length "); Put( integer(openssl.evp.cipher.Key_Length(cip))); New_Line ;
   --  Put("IV Length "); Put(Integer(openssl.evp.cipher.Iv_length(cip))); New_Line ;
   --  Put_Line("Generating randomized IV");
   --
   --  iv := openssl.evp.cipher.Generate(Integer(openssl.evp.cipher.Iv_Length(cip))) ;
   --  openssl.evp.cipher.Show(iv);
   --

   status := openssl.evp.cipher.Init_crypto(OPENSSL.OPENSSL_INIT_LOAD_CRYPTO_STRINGS);
   Put("Load Crypto Strings Status "); Put(integer(status)); New_Line ;

   status := openssl.evp.cipher.Init_crypto(OPENSSL.OPENSSL_INIT_ADD_ALL_CIPHERS or
                                              OPENSSL.OPENSSL_INIT_ADD_ALL_DIGESTS ) ;
   Put("Add all Ciphers etc "); Put(integer(status)); New_Line ;

   openssl.Config(Interfaces.C.Strings.Null_Ptr);
   declare
      iv : InitVector_Type := openssl.evp.cipher.util.Encrypt("ssldigest.exe"
                                                              , "ssldigest.exe.enc"
                                                              , "Key"
                                                              , algname);
   begin
      openssl.evp.cipher.util.Decrypt( "ssldigest.exe.enc" ,
                                       "ssldigest.exe.dec" ,
                                       "Key" ,
                                       iv ,
                                       algname ) ;
   end ;

end Sslcrypt;
