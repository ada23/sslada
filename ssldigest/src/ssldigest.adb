with Ada.Text_IO; use Ada.Text_IO;
with GNAT.Command_Line ;

with cli ;
with openssl.evp.digest.util ;

procedure ssldigest is
begin
   cli.ProcessCommandLine ;   
   Put_Line(cli.algorithm.all);
   loop 
      declare
         next : string := cli.GetNextArgument ;
      begin
         if next'length < 1
         then
            exit ;
         end if;
         Put(openssl.evp.digest.util.Digest(next,cli.algorithm.all));
         Put(" : ");
         Put(next);
         New_Line;
      end ;
   end loop ;
   
exception
   when GNAT.Command_Line.Exit_From_Command_Line =>
      return ;
end ssldigest;
