
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package cli is

   VERSION : string := "V01" ;
   NAME : String := "ssldigest" ;
   Verbose : aliased boolean ;

   HelpOption : aliased boolean ;
   DebugOption : aliased boolean ;
   algorithms : String :=
     "-blake2b512 " &
     "-blake2s256 " &
     "-md4 " &
     "-md5 " &
     "-md5-sha1 " &
     "-mdc2 " &
     "-ripemd " &
     "-ripemd160 " &
     "-rmd160 " &
     "-sha1 " &
     "-sha224 " &
     "-sha256 " &
     "-sha3-224 " &
     "-sha3-256 " &
     "-sha3-384 " &
     "-sha3-512 " &
     "-sha384 " &
     "-sha512 " &
     "-sha512-224 " &
     "-sha512-256 " &
     "-shake128 " &
     "-shake256 " &
     "-sm3 " &
     "-ssl3-md5 " &
     "-ssl3-sha1 " &
     "-whirlpool " ;
   md5 : aliased String := "md5" ;
   algorithm : aliased gnat.strings.String_Access ; -- := md5'Access ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

   function Get(prompt : string) return String ;

end cli;
