package openssl.evp.cipher.util is

   procedure Initialize( algorithm : String ) ;
   function Encrypt( filename : string ; outfilename : string ;
                      key : string ;
                     algorithm : string := "seed-ofb" )
                    return InitVector_Type ;

   procedure Decrypt( filename : string ; outfilename : string ;
                      key : string ;
                      iv : InitVector_Type ;
                      algorithm : string := "idea-ecb" );

end openssl.evp.cipher.util;
