package openssl.evp.digest.util is

   procedure Initialize( algorithm : String ) ;
   function Digest( filename : string ;
                    algorithm : string := "md5" )
     return String ;

end openssl.evp.digest.util;
