with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with Ada.Streams.Stream_IO;
package body openssl.evp.cipher.util is

   libctx      : openssl.Context;
   cip         : CIPHER_ALGORITHM;
   initialized : Boolean := False;
   status      : Interfaces.C.int;

   procedure Initialize (algorithm : String) is
   begin
      if initialized then
         raise Program_Error with "Already Initialized";
      end if;

      libctx := openssl.LibraryContext;
      if libctx = openssl.NullContext then
         raise Program_Error with "LibraryContext";
      end if;

      cip := openssl.evp.cipher.CipherByName (Interfaces.C.To_C (algorithm));
      if cip = openssl.evp.cipher.NullCipher then
         raise Program_Error with "CipherLookup " & algorithm;
      end if;

      initialized := True;
      Put ("Initialized - algorithm ");
      Put_Line (algorithm);
   end Initialize;

   function Encrypt
     (filename  : String; outfilename : String;
      key : String;
      algorithm : String := "seed-ofb")
     return InitVector_Type
   is
      use Ada.Streams;
      ctx : Context := NewContext;
      iv  : InitVector_Type;

      f    : Ada.Streams.Stream_IO.File_Type;
      outf : Ada.Streams.Stream_IO.File_Type;
   begin
      if not initialized then
         Initialize (algorithm);
      end if;
      Put ("Block Size ");
      Put (Integer (openssl.evp.cipher.Block_size (cip)));
      New_Line;
      Put ("Key Length ");
      Put (Integer (openssl.evp.cipher.Key_length (cip)));
      New_Line;
      Put ("IV Length ");
      Put (Integer (openssl.evp.cipher.Iv_length (cip)));
      New_Line;

      iv := Generate (Integer (Iv_length (cip)));
      declare
         buffer : Stream_Element_Array
           (1 .. Stream_Element_Offset (Block_size (cip)));
         bufbytes : Stream_Element_Count;
         bytes    : Stream_Element_Count := 0;

         outbuf : Stream_Element_Array
           (1 .. Stream_Element_Offset (64 * Block_size (cip)));
         outbuflen : aliased int;
      begin

         Put (filename);
         Put ("<");
         Show (iv);

         status := EncryptInit (ctx, cip, key, iv);
         if status /= 1 then
            ReportError (status);
            raise Program_Error with "EncryptInit";
         end if;

         Stream_IO.Open (f, Ada.Streams.Stream_IO.In_File, filename);
         Stream_IO.Create (outf, Ada.Streams.Stream_IO.Out_File, outfilename);
         while not Ada.Streams.Stream_IO.End_Of_File (f) loop
            Stream_IO.Read (f, buffer, bufbytes);
            --Put("Read "); Put(Integer(bufbytes)); Put_Line(" bytes");
            outbuflen := 64;
            status    :=
              EncryptUpdate
                (ctx, outbuf'address, outbuflen'access, buffer'address,
                 int (bufbytes));
            if status /= 1 then
               ReportError (status);
               -- Put("EncryptUpdate error "); Put(Integer(status)); New_Line ;
               raise Program_Error with "EncryptBlock";
            end if;
            Stream_IO.Write
              (outf, outbuf (1 .. Stream_Element_Count (outbuflen)));
            --  Put ("Wrote ");
            --  Put (Integer (outbuflen));
            --  Put_Line (" bytes");
         end loop;
         status := EncryptFinal_ex (ctx, outbuf'address, outbuflen'access);
         --  Put ("(Final) Wrote ");
         --  Put (Integer (outbuflen));
         --  Put_Line (" bytes");
         if status /= 1 then
            ReportError (status); -- raise Program_Error with "EncryptFinal" ;
         end if;
         Stream_IO.Write
           (outf, outbuf (1 .. Stream_Element_Count (outbuflen)));
      end;
      Stream_IO.Close (f);
      Stream_IO.Close (outf);
      Free (ctx);
      return iv ;
   end Encrypt;

   procedure Decrypt
     (filename : String; outfilename : String; key : String;
      iv       : InitVector_Type; algorithm : String := "idea-ecb")
   is
      use Ada.Streams;
      ctx  : Context := NewContext;
      f    : Ada.Streams.Stream_IO.File_Type;
      outf : Ada.Streams.Stream_IO.File_Type;

   begin
      if not initialized then
         Initialize (algorithm);
      end if;
      status := DecryptInit (ctx, cip, key, iv);
      if status /= 1 then
         ReportError (status);
         raise Program_Error with "DecryptInit";
      end if;
      declare
         buffer : Stream_Element_Array
           (1 .. Stream_Element_Offset (Block_size (cip)));
         bufbytes : Stream_Element_Count;
         bytes    : Stream_Element_Count := 0;

         outbuf : Stream_Element_Array
           (1 .. Stream_Element_Offset (64 * Block_size (cip)));
         outbuflen : aliased int;
      begin
         Stream_IO.Open (f, Ada.Streams.Stream_IO.In_File, filename);
         Stream_IO.Create (outf, Ada.Streams.Stream_IO.Out_File, outfilename);
         while not Ada.Streams.Stream_IO.End_Of_File (f) loop
            Stream_IO.Read (f, buffer, bufbytes);
            --Put("Read "); Put(Integer(bufbytes)); Put_Line(" bytes");
            outbuflen := 64;
            status    :=
              DecryptUpdate
                (ctx, outbuf'address, outbuflen'access, buffer'address,
                 int (bufbytes));
            if status /= 1 then
               ReportError (status);
               -- Put("EncryptUpdate error "); Put(Integer(status)); New_Line ;
               raise Program_Error with "DecryptBlock";
            end if;
            Stream_IO.Write
              (outf, outbuf (1 .. Stream_Element_Count (outbuflen)));
            --  Put ("Wrote ");
            --  Put (Integer (outbuflen));
            --  Put_Line (" bytes");
         end loop;
         status := DecryptFinal_ex (ctx, outbuf'address, outbuflen'access);
         Put ("(Final) Wrote ");
         Put (Integer (outbuflen));
         Put_Line (" bytes");
         if status /= 1 then
            ReportError (status); -- raise Program_Error with "EncryptFinal" ;
         end if;
         Stream_IO.Write
           (outf, outbuf (1 .. Stream_Element_Count (outbuflen)));
      end;
      Stream_IO.Close (f);
      Stream_IO.Close (outf);
      Free (ctx);

   end Decrypt;

end openssl.evp.cipher.util;
