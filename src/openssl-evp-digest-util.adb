with System.Unsigned_Types ;
with Ada.Streams.Stream_Io ; use Ada.Streams.Stream_Io ;
with Ada.Text_Io; use Ada.Text_Io;
with Interfaces.C ; use Interfaces.C ;

with hex ;

package body openssl.evp.digest.util is

   BLOCKSIZE : constant Integer := 1024 ;
   initialized : boolean := false ;
   
   ctx : openssl.Context ;
   dig : MessageDigest ;

   status : Interfaces.C.int ;
   
   procedure Initialize( algorithm : String ) is
   begin
      if initialized
      then
         raise Program_Error with "Already Initialized" ;
      end if ;
      
      ctx := openssl.LibraryContext;
      if ctx = openssl.NullContext
      then
         raise Program_Error with "LibraryContext" ;
      end if ;
   
      dig := DigestByName( Interfaces.C.To_C(algorithm) ) ;
      if dig = NullMessageDigest
      then
         raise Program_Error with "MessageDigest" ;
      end if ;
      
      initialized := true ;
      --Put("Initialized - algorithm "); Put_Line(algorithm);
   end Initialize ;
   
   function Digest( filename : string ;
                    algorithm : string := "md5" )
                   return String is

      digest : array (1..EVP_MAX_MD_SIZE) of System.Unsigned_Types.Short_Short_Unsigned ;
      diglen : aliased Interfaces.C.unsigned := 0 ;

      
      function digest_file( filename : string ) return String is
         use Ada.Streams ;
         f : Ada.Streams.Stream_Io.File_Type ;
         buffer : ada.Streams.Stream_Element_Array(1..Ada.Streams.Stream_Element_Offset(BLOCKSIZE) ) ;
         bufbytes : ada.Streams.Stream_Element_Count ;
         bytes : ada.Streams.Stream_Element_Count := 0 ;
         mdctx : Context ;
      begin
         mdctx := NewContext ;
         status := Initialize(mdctx,dig) ;
         if status /= 1
         then
            raise Program_Error with "InitializeMDContext" ;
         end if;
         
         
         Open(f , ada.streams.Stream_IO.In_File , filename ) ;
         while not ada.Streams.Stream_IO.End_Of_File(f)
         loop
            Read(f,buffer,bufbytes) ;
            status := Update(mdctx,buffer'Address,
                                         Interfaces.C.size_t(bufbytes)) ;
            if status /= 1
            then
               raise Program_Error with "DigestUpdate" ;
            end if ;
            
            bytes := bytes + bufbytes ;
         end loop ;
         Close(f) ;
   
         status := Finalize(mdctx,digest'Address,diglen'Access);
         status := Reset(mdctx) ;
         if status /= 1
         then
            Put_Line("Failed to reset mdctx");
         end if ;
         
         Free(mdctx) ;
         
         return hex.Image(digest'Address,Integer(diglen)) ;

    end digest_file ;

   begin
      if not initialized
      then
         Initialize( algorithm ) ;
      end if ;
      
      return digest_file(filename) ;
   end Digest ;
   
end openssl.evp.digest.util;
