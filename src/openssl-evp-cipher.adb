with ada.text_io; use ada.text_io ;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with system.Unsigned_Types; use system.Unsigned_Types ;
with gnat.Random_Numbers ;
with hex ;
package body openssl.evp.cipher is

   function cipherbyname( name : string ) return CIPHER_ALGORITHM is
   begin
      return cipherbyname( interfaces.c.To_C(name) ) ;
   end cipherbyname ;
   
   gen : gnat.Random_Numbers.Generator ;
   function Random_Byte is new GNAT.Random_Numbers.Random_Discrete( Packed_Byte ) ;
   function Generate(iv_length : integer)  return InitVector_Type is
      iv : InitVector_Type := new InitializationVector (1..iv_length);
   begin

      for ivp in 1..iv_length
      loop
         iv(ivp) := Random_Byte(gen) ;
      end loop ;
      return iv ;
   end Generate ;
   procedure Show( iv : InitVector_Type ) is
   begin
      hex.dump(iv.all'address,iv.all'length) ;
      new_line ;
   end Show ;
   
   function EncryptInit( ctx : Context ; 
                          cipher : CIPHER_ALGORITHM ;
                          key : Key_Type ;
                          iv : access InitializationVector ) return int is
   begin
      return EncryptInit_Ex( ctx , cipher , NullEngine , key.all'Address , iv.all'Address ) ;
   end EncryptInit ;
   
   function EncryptInit( ctx : Context ; 
                         cipher : CIPHER_ALGORITHM ;
                         key : String ;
                         iv : access InitializationVector ) 
                        return int is
      K : Key_Type := new system.Unsigned_Types.Packed_Bytes1( 1 .. Integer(Key_Length(cipher))) ;
      KS : String(1..Integer(Key_Length(cipher)));
      for KS'address use k.all'address ;
   begin
      KS := (Others => ASCII.NUL) ;
      if key'length <= KS'length
      then
         KS(1..Key'length) := Key ;
      else
         KS := Key(Key'First..KS'Last) ;
      end if ;
      --Put("Key "); Put(key); Put(" normalized to "); Put(KS'length); Put_Line(" bytes");
      return EncryptInit(ctx, cipher , K , Iv) ;
   end EncryptInit ;
   function DecryptInit( ctx : Context ; 
                          cipher : CIPHER_ALGORITHM ;
                          key : Key_Type ;
                          iv : InitVector_Type ) 
                        return int is
   begin
      return DecryptInit_Ex(ctx,cipher,NullEngine,key.all'address, iv.all'address);
   end DecryptInit ;
   
   function DecryptInit( ctx : Context ; 
                         cipher : CIPHER_ALGORITHM ;
                         key : String ;
                         iv : InitVector_Type ) 
                        return int is
      K : Key_Type := new system.Unsigned_Types.Packed_Bytes1( 1 .. Integer(Key_Length(cipher))) ;
      KS : String(1..Integer(Key_Length(cipher)));
      for KS'address use k.all'address ;
   begin
      KS := (Others => ASCII.NUL) ;
      if key'length <= KS'length
      then
         KS(1..Key'length) := Key ;
      else
         KS := Key(Key'First..KS'Last) ;
      end if ;
      return DecryptInit(ctx,cipher,K,IV);
   end DecryptInit ;
   
begin
   gnat.Random_Numbers.Reset(gen);
end openssl.evp.cipher;
