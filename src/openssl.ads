with System ;
with Interfaces.C.Strings ; use Interfaces.C.Strings ;

package openssl is
   type OSSL_LIB_CTX is new System.Address ;
   subtype Context is OSSL_LIB_CTX ;
   NullContext : constant Context := Context(System.Null_Address) ;
   function LibraryContext return Context  -- ../openssl/crypto.h:541
   with Import => True, 
        Convention => C, 
     External_Name => "OSSL_LIB_CTX_new";
   
   OPENSSL_INIT_NO_LOAD_CRYPTO_STRINGS : constant := 16#00000001#;  --  ../openssl/crypto.h:444
   OPENSSL_INIT_LOAD_CRYPTO_STRINGS : constant := 16#00000002#;  --  ../openssl/crypto.h:445
   OPENSSL_INIT_ADD_ALL_CIPHERS : constant := 16#00000004#;  --  ../openssl/crypto.h:446
   OPENSSL_INIT_ADD_ALL_DIGESTS : constant := 16#00000008#;  --  ../openssl/crypto.h:447
   OPENSSL_INIT_NO_ADD_ALL_CIPHERS : constant := 16#00000010#;  --  ../openssl/crypto.h:448
   OPENSSL_INIT_NO_ADD_ALL_DIGESTS : constant := 16#00000020#;  --  ../openssl/crypto.h:449
   OPENSSL_INIT_LOAD_CONFIG : constant := 16#00000040#;  --  ../openssl/crypto.h:450
   OPENSSL_INIT_NO_LOAD_CONFIG : constant := 16#00000080#;  --  ../openssl/crypto.h:451
   OPENSSL_INIT_ASYNC : constant := 16#00000100#;  --  ../openssl/crypto.h:452
   OPENSSL_INIT_ENGINE_RDRAND : constant := 16#00000200#;  --  ../openssl/crypto.h:453
   OPENSSL_INIT_ENGINE_DYNAMIC : constant := 16#00000400#;  --  ../openssl/crypto.h:454
   OPENSSL_INIT_ENGINE_OPENSSL : constant := 16#00000800#;  --  ../openssl/crypto.h:455
   OPENSSL_INIT_ENGINE_CRYPTODEV : constant := 16#00001000#;  --  ../openssl/crypto.h:456
   OPENSSL_INIT_ENGINE_CAPI : constant := 16#00002000#;  --  ../openssl/crypto.h:457
   OPENSSL_INIT_ENGINE_PADLOCK : constant := 16#00004000#;  --  ../openssl/crypto.h:458
   OPENSSL_INIT_ENGINE_AFALG : constant := 16#00008000#;  --  ../openssl/crypto.h:459

   OPENSSL_INIT_ATFORK : constant := 16#00020000#;  --  ../openssl/crypto.h:461

   OPENSSL_INIT_NO_ATEXIT : constant := 16#00080000#;  --  ../openssl/crypto.h:463
   
   type OPENSSL_INIT_SETTINGS is new System.Address ;
   NullOpenSSLInitSettings : constant OPENSSL_INIT_SETTINGS := 
     OPENSSL_INIT_SETTINGS(System.Null_Address) ;
  
   procedure Config (config_name : Interfaces.C.Strings.chars_ptr)  -- ../openssl/conf.h:141
   with Import => True, 
        Convention => C, 
     External_Name => "OPENSSL_config";
   
   function INIT_new return OPENSSL_INIT_SETTINGS  -- ../openssl/crypto.h:487
   with Import => True, 
        Convention => C, 
     External_Name => "OPENSSL_INIT_new";

   procedure INIT_free (settings : OPENSSL_INIT_SETTINGS)  -- ../openssl/crypto.h:496
   with Import => True, 
        Convention => C, 
     External_Name => "OPENSSL_INIT_free";
   
   procedure Cleanup  -- ../openssl/crypto.h:480
   with Import => True, 
        Convention => C, 
     External_Name => "OPENSSL_cleanup";
   
end openssl;
