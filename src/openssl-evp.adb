with Ada.Text_Io; use Ada.TExt_Io ;
with Ada.Integer_Text_IO; use ada.Integer_Text_IO;
package body openssl.evp is
   procedure ReportError(e : int) is
   begin
      print_errors_fp( Interfaces.C_Streams.stderr );
      Put("Error Code "); Put(integer(e)); New_Line ;
      --Put_Line(Value(lib_error_string(unsigned_long(e))));
   end ReportError ;
end openssl.evp ;
