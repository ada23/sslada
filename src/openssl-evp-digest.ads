with System.Unsigned_Types ;
package openssl.evp.digest is

   EVP_MAX_MD_SIZE : constant := 64;  --  ../openssl/evp.h:34
   type DigestValue is array (integer range <>) of System.Unsigned_Types.Packed_Byte ;
   
   type EVP_MD is new System.Address ;
   subtype MessageDigest is EVP_MD ;
   NullMessageDigest : constant MessageDigest := MessageDigest(System.Null_Address);
   
   function Fetch
     (ctx : Context ;
      algorithm : Interfaces.C.char_array ;
      properties : Interfaces.C.char_array ) 
      return MessageDigest  -- ../openssl/evp.h:687
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_MD_fetch";
   
   function DigestByName (name : Interfaces.C.char_array ) 
                                  return MessageDigest  -- ../openssl/evp.h:1131
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_get_digestbyname";
   
   function Size (md : MessageDigest) return int  -- ../openssl/evp.h:537
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_MD_size";
   function Block_Size (md : MessageDigest) return int  -- ../openssl/evp.h:538
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_MD_block_size";
   
   type EVP_MD_CTX is new System.Address ;
   subtype Context is EVP_MD_CTX ;
   NullContext : Context := Context( System.Null_Address ) ;
   
   function NewContext return Context  -- ../openssl/evp.h:658
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_MD_CTX_new";
   
   
   function Reset (ctx : Context) return int  -- ../openssl/evp.h:659
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_MD_CTX_reset";

   
   function Initialize
     (ctx : Context ;
      c_type : MessageDigest ) return Interfaces.C.int  -- ../openssl/evp.h:670
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_DigestInit";
   
   function Update
     (ctx : Context ;
      d : System.Address;
      cnt : Interfaces.C.size_t ) return Interfaces.C.int  -- ../openssl/evp.h:672
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_DigestUpdate";
   
   function Finalize
     (ctx : Context ;
      md : System.Address ;
      s : access Interfaces.C.unsigned ) return Interfaces.C.int  -- ../openssl/evp.h:674
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_DigestFinal";
   
   procedure Free (ctx : Context)  -- ../openssl/evp.h:660
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_MD_CTX_free";
     
end openssl.evp.digest;
