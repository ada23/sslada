with system.unsigned_types ; use system.Unsigned_Types ;
with Interfaces.C ; use Interfaces.C ;
with interfaces.C ; use Interfaces.C ;

package openssl.evp.cipher is

   EVP_MAX_KEY_LENGTH : constant := 64;  --  ../openssl/evp.h:35
   EVP_MAX_IV_LENGTH : constant := 16;  --  ../openssl/evp.h:36
   EVP_MAX_BLOCK_LENGTH : constant := 32;  --  ../openssl/evp.h:37
   
   
   function Init_crypto (opts : interfaces.c.unsigned_long_long ; 
                                 settings : OPENSSL_INIT_SETTINGS := NullOpenSSLInitSettings ) 
                                 return int  -- ../openssl/crypto.h:481
   with Import => True, 
        Convention => C, 
     External_Name => "OPENSSL_init_crypto";
   
   type EVP_CIPHER_CTX is new System.Address ;
   subtype Context is EVP_CIPHER_CTX ;
   NullContext : constant Context := Context(System.Null_Address) ;
   
   type CIPHER_ALGORITHM is new System.Address ;
   NullCipher : constant CIPHER_ALGORITHM := CIPHER_ALGORITHM(System.Null_Address);
   
   type ENGINE is new System.Address ;
   NullEngine : constant ENGINE := ENGINE(System.Null_Address) ;
 
   type InitializationVector is array (integer range <>) of 
     System.Unsigned_Types.Packed_Byte ;
 
   -- type InitVector_Type is access system.unsigned_types.Packed_Bytes ;
   type InitVector_Type is access all InitializationVector ;
   
   type Key_Type is access system.unsigned_types.Packed_Bytes1 ;
   
   function NewContext return Context -- ../openssl/evp.h:832
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_CIPHER_CTX_new";
   
   function Reset (c : Context ) return int  -- ../openssl/evp.h:833
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_CIPHER_CTX_reset";

   procedure Free (c : Context)  -- ../openssl/evp.h:834
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_CIPHER_CTX_free";
  
   function EncryptInit
     (ctx : Context ;
      cipher : CIPHER_ALGORITHM;
      key : access unsigned_char;
      iv : access unsigned_char) return int  -- ../openssl/evp.h:708
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_EncryptInit";

   function EncryptInit_ex
     (ctx : Context ;
      cipher : CIPHER_ALGORITHM ;
      impl : ENGINE ;
      key : System.Address ;
      iv : System.Address ) return int  -- ../openssl/evp.h:710
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_EncryptInit_ex";
  
   function EncryptInit( ctx : Context ; 
                         cipher : CIPHER_ALGORITHM ;
                         key : String ;
                         iv : access InitializationVector ) 
                         return int ;
   
   function EncryptInit( ctx : Context ; 
                          cipher : CIPHER_ALGORITHM ;
                          key : Key_Type ;
                          iv : access InitializationVector ) 
                        return int ;
   
   function EncryptUpdate
     (ctx : Context;
      c_out : System.address ;
      outl : access int ;
      c_in : System.Address ;
      inl : int) return int  -- ../openssl/evp.h:718
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_EncryptUpdate";
 
   function EncryptFinal
     (ctx : Context ;
      c_out : System.Address ;
      outl : access int) return int  -- ../openssl/evp.h:722
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_EncryptFinal";
   
   function EncryptFinal_ex
     (ctx : Context ;
      c_out : System.Address ;
      outl : access int) return int  -- ../openssl/evp.h:720
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_EncryptFinal_ex";
  
   
   function DecryptInit
     (ctx : Context ;
      cipher : CIPHER_ALGORITHM ;
      key : access unsigned_char;
      iv : access unsigned_char) return int  -- ../openssl/evp.h:725
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_DecryptInit";

   function DecryptInit_ex
     (ctx : Context ;
      cipher : CIPHER_ALGORITHM ;
      impl : ENGINE;
      key : System.Address ;
      iv : System.Address ) return int  -- ../openssl/evp.h:727
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_DecryptInit_ex";
   
   
   function DecryptInit( ctx : Context ; 
                         cipher : CIPHER_ALGORITHM ;
                         key : String ;
                         iv : InitVector_Type ) 
                         return int ;
   
   function DecryptInit( ctx : Context ; 
                          cipher : CIPHER_ALGORITHM ;
                          key : Key_Type ;
                          iv : InitVector_Type ) 
                        return int ;
   function DecryptUpdate
     (ctx : Context ;
      c_out : System.Address ;
      outl : access int;
      c_in : System.Address ;
      inl : int) return int  -- ../openssl/evp.h:735
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_DecryptUpdate";

   function DecryptFinal
     (ctx : Context ;
      outm : System.Address ;
      outl : access int) return int  -- ../openssl/evp.h:737
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_DecryptFinal";
   
   function DecryptFinal_ex
     (ctx : Context ;
      outm : System.Address ;
      outl : access int) return int  -- ../openssl/evp.h:739
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_DecryptFinal_ex";
   
   function CipherByName (name : Interfaces.C.char_array) return CIPHER_ALGORITHM  -- ../openssl/evp.h:1130
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_get_cipherbyname";
   
   function cipherbyname( name : string ) return CIPHER_ALGORITHM ;
   
   function idea_ecb return CIPHER_ALGORITHM  -- ../openssl/evp.h:935
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_idea_ecb";

   function idea_cfb64 return CIPHER_ALGORITHM  -- ../openssl/evp.h:936
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_idea_cfb64";

   function idea_ofb return CIPHER_ALGORITHM  -- ../openssl/evp.h:938
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_idea_ofb";

   function idea_cbc return CIPHER_ALGORITHM  -- ../openssl/evp.h:939
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_idea_cbc";
   
  function Block_size (cipher : CIPHER_ALGORITHM) return int  -- ../openssl/evp.h:571
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_CIPHER_block_size";
   
   function Key_length (cipher : CIPHER_ALGORITHM) return int  -- ../openssl/evp.h:573
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_CIPHER_key_length";
 
   function Iv_length (cipher : CIPHER_ALGORITHM ) return int  -- ../openssl/evp.h:574
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_CIPHER_iv_length";
   
   function Flags (cipher : CIPHER_ALGORITHM) return unsigned_long  -- ../openssl/evp.h:575
   with Import => True, 
        Convention => C, 
        External_Name => "EVP_CIPHER_flags";

   function Mode (cipher : CIPHER_ALGORITHM) return int  -- ../openssl/evp.h:576
   with Import => True, 
        Convention => C, 
     External_Name => "EVP_CIPHER_mode";

   function Generate(iv_length : integer) return InitVector_Type ;
   procedure Show( iv : InitVector_Type ) ;
end openssl.evp.cipher;
