with Interfaces.C ; use Interfaces.C ;
with INterfaces.C.Strings ; use Interfaces.C.Strings ;
with Interfaces.C_Streams ;

package openssl.evp is


   PKCS5_SALT_LEN : constant := 8;  --  ../openssl/evp.h:39

   PKCS5_DEFAULT_ITER : constant := 2048;  --  ../openssl/evp.h:41

   EVP_PK_RSA : constant := 16#0001#;  --  ../openssl/evp.h:46
   EVP_PK_DSA : constant := 16#0002#;  --  ../openssl/evp.h:47
   EVP_PK_DH : constant := 16#0004#;  --  ../openssl/evp.h:48
   EVP_PK_EC : constant := 16#0008#;  --  ../openssl/evp.h:49
   EVP_PKT_SIGN : constant := 16#0010#;  --  ../openssl/evp.h:50
   EVP_PKT_ENC : constant := 16#0020#;  --  ../openssl/evp.h:51
   EVP_PKT_EXCH : constant := 16#0040#;  --  ../openssl/evp.h:52
   EVP_PKS_RSA : constant := 16#0100#;  --  ../openssl/evp.h:53
   EVP_PKS_DSA : constant := 16#0200#;  --  ../openssl/evp.h:54
   EVP_PKS_EC : constant := 16#0400#;  --  ../openssl/evp.h:55
        
   procedure ReportError(e : int);
   
   function lib_error_string (e : unsigned_long) return Interfaces.C.Strings.chars_ptr  -- ../openssl/err.h:447
   with Import => True, 
        Convention => C, 
        External_Name => "ERR_lib_error_string";

   function func_error_string (e : unsigned_long) return Interfaces.C.Strings.chars_ptr  -- ../openssl/err.h:449
   with Import => True, 
        Convention => C, 
        External_Name => "ERR_func_error_string";

   function reason_error_string (e : unsigned_long) return Interfaces.C.Strings.chars_ptr  -- ../openssl/err.h:451
   with Import => True, 
        Convention => C, 
     External_Name => "ERR_reason_error_string";
   
   procedure print_errors_fp (fp : Interfaces.C_Streams.FILEs)  -- ../openssl/err.h:456
   with Import => True, 
        Convention => C, 
     External_Name => "ERR_print_errors_fp";
   
end openssl.evp;
